import { useState, useEffect } from "react";
import api from "../../services/api";
import { Redirect } from "react-router-dom";
import UserInfo from "../../components/UserInfo";

const Home = ({ setIsAuth }) => {
  const [user, setUser] = useState({});
  const [token, setToken] = useState(() => {
    const sessionToken = sessionStorage.getItem("token") || ""; //undefined ? ""
    if (!sessionToken) {
      return JSON.parse(sessionToken);
    }
    setIsAuth(true);
    return JSON.parse(sessionToken);
  });

  useEffect(() => {
    api
      .get("/profile", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => setUser(response.data))
      .catch((e) => console.log(e));
  }, []);

  if (!token) {
    return <Redirect to="/register" />;
  }

  const { name, email, course_module, bio, contact, techs, avatar_url } = user;
  return (
    <div>
      <UserInfo
        name={name}
        email={email}
        course_module={course_module}
        bio={bio}
        contact={contact}
        techs={techs}
        avatar_url={avatar_url}
      />
    </div>
  );
};

export default Home;
