const NotFound = () => <h3>404 - Page is not found</h3>;

export default NotFound;
