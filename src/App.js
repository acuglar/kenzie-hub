import Routes from './routes'
import Menu from './components/Menu'
import { useState } from 'react'

function App() {
  const [isAuth, setIsAuth] = useState(false);


  return (
    <div className="App">
      <Menu isAuth={isAuth} setIsAuth={setIsAuth} />
      <header className="App-header">
        <Routes setIsAuth={setIsAuth} />
      </header>
    </div>
  );
}

export default App;
