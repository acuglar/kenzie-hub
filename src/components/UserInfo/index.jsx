import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import UserInfoTechs from "../UserInfoTechs";

const useStyles = makeStyles({
  root: {
    margin: 40,
    minWidth: 275,
    maxWidth: 400,
  },
  bullet: {
    display: "inline-block",
    margin: "0 4px",
  },
  pos: {
    marginBottom: 12,
  },
});

const UserInfo = ({ avatar_url, name, email, course_module, bio, contact }) => {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  const [showTech, setShowTech] = useState(false);

  return (
    <Card className={classes.root}>
      <CardContent>
        {avatar_url}
        <Typography variant="h5" component="h2">
          {name}
          {bull}
          {email}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          Módulo do curso: {course_module}
        </Typography>
        <Typography variant="body1" component="p">
          bio: {bio}
          <br />
          Contato: {contact}
        </Typography>
      </CardContent>
      <CardActions>
        <Button onClick={() => setShowTech(!showTech)} size="large">
          TECNOLOGIAS
        </Button>
        {showTech && <UserInfoTechs />}
      </CardActions>
    </Card>
  );
};

export default UserInfo;
