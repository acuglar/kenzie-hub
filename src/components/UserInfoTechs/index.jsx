import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import api from "../../services/api";
import { makeStyles } from "@material-ui/core/styles";
import {
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Button,
  TextField,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(0, 0, 0),
  },
}));

const UserInfoTechs = () => {
  const classes = useStyles();
  const [techList, setTechList] = useState([]);
  const [addTech, setAddTech] = useState(false);

  const schema = yup.object().shape({
    title: yup.string().required("Campo Obrigatório"),
    status: yup.string().required("Campo Obrigatório"),
  });

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const handleTech = (data) => {
    console.log(data);
    api
      .post("/users/techs", { ...data })
      .then((response) => {
        localStorage.setItem("token", JSON.stringify(response.data.token));
        setTechList(...response.data);
      })
      .catch((e) => console.log(e));
  };

  const AddTech = () => {
    return (
      <form onSubmit={handleSubmit(handleTech)}>
        <div style={{ margin: "10px 0" }}>
          <input placeholder="Tecnologia" name="title" ref={register} />
        </div>

        <div style={{ margin: "10px 0" }}>
          <input
            placeholder="Nível de conhecimento"
            list="list"
            name="status"
            id="datalist"
            ref={register}
          />
          <datalist id="list">
            <option value="Iniciante" />
            <option value="Intermediário" />
            <option value="Avançado" />
          </datalist>
        </div>
        <div>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            size="small"
          >
            Adicionar
          </Button>
        </div>
      </form>
    );
  };

  const Tech = (props) => {
    return (
      <div className={classes.demo}>
        <List>
          <ListItem className={classes.title}>
            <ListItemText primary={props.primary} secondary={props.secondary} />
            <ListItemSecondaryAction />
            <Button edge="end" aria-label="delete" size="small">
              Excluir
            </Button>
          </ListItem>
        </List>
      </div>
    );
  };

  const InfoTech = (props) => {
    return (
      <div>
        {techList.map((value, index) => (
          <Tech
            key={index}
            primary={value}
            secondary="props.AddTech.datalist"
          />
        ))}
        <Button onClick={() => setAddTech(!addTech)} size="small">
          Add
        </Button>
        {addTech && <AddTech />}
      </div>
    );
  };

  return <InfoTech />;
};

export default UserInfoTechs;
