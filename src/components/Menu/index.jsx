import { AppBar, Toolbar, MenuItem } from "@material-ui/core/";

import { useHistory } from "react-router-dom";

const Menu = ({ isAuth, setIsAuth }) => {
  const history = useHistory();

  const sendTo = (path) => {
    history.push(path);
  };

  const handleCloseApplication = () => {
    sessionStorage.clear();
    setIsAuth(false);
    sendTo("/");
  };

  return (
    <AppBar position="static">
      <Toolbar>
        {isAuth ? (
          <>
            <MenuItem onClick={() => sendTo("/home")}>Home</MenuItem>
            <MenuItem onClick={handleCloseApplication}>Logout</MenuItem>
          </>
        ) : (
          <>
            <MenuItem onClick={() => sendTo("/")}>Sign in</MenuItem>
            <MenuItem onClick={() => sendTo("/register")}>Sign up</MenuItem>
          </>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default Menu;
