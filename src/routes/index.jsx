import Signin from "../pages/SignIn";
import Signup from "../pages/SignUp";
import Home from "../pages/Home";
import NotFound from "../pages/NotFound";

import { Switch, Route } from "react-router-dom";

const Routes = ({ setIsAuth }) => {
  return (
    <Switch>
      <Route exact path="/">
        <Signin />
      </Route>
      <Route path="/register">
        <Signup />
      </Route>
      <Route path="/home">
        <Home setIsAuth={setIsAuth} />
      </Route>
      <Route>
        <NotFound />
      </Route>
    </Switch>
  );
};

export default Routes;
